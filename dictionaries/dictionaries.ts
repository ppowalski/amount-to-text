import { Dictionary as DictionaryPL } from "./langs/pl/Dictionary";
import { Dictionary as DictionaryEN } from "./langs/en/Dictionary";

export default {
    PL: new DictionaryPL,
    EN: new DictionaryEN
}