import { DictionaryInterface } from "../../DictionaryInterface";
import { DictionaryItemInterface } from "../../DictionaryItemInterface";
import { NumberBaseItem } from "../../numbers/NumberBaseItem";
import { NumberAdvancedItem } from "../../numbers/NumberAdvancedItem";
import { Thousand } from "./Thousand";
import { Million } from "./Million";
import { Billion } from "./Billion";
import { Trillion } from "./Trillion";

export class Dictionary implements DictionaryInterface {
    getItems(): DictionaryItemInterface[] {
        return [
            new NumberBaseItem('zero', 0),
            new NumberBaseItem('jeden', 1),
            new NumberBaseItem('dwa', 2),
            new NumberBaseItem('trzy', 3),
            new NumberBaseItem('cztery', 4),
            new NumberBaseItem('pięć', 5),
            new NumberBaseItem('sześć', 6),
            new NumberBaseItem('siedem', 7),
            new NumberBaseItem('osiem', 8),
            new NumberBaseItem('dziewięć', 9),

            new NumberBaseItem('dziesięć', 10),
            new NumberBaseItem('jedenaście', 11),
            new NumberBaseItem('dwanaście', 12),
            new NumberBaseItem('trzynaście', 13),
            new NumberBaseItem('czternaście', 14),
            new NumberBaseItem('piętnaście', 15),
            new NumberBaseItem('szesnaście', 16),
            new NumberBaseItem('siedemnaście', 17),
            new NumberBaseItem('osiemnaście', 18),
            new NumberBaseItem('dziewiętnaście', 19),

            new NumberBaseItem('dwadzieścia', 20),
            new NumberBaseItem('trzydzieści', 30),
            new NumberBaseItem('czterdzieści', 40),
            new NumberBaseItem('pięćdziesiąt', 50),
            new NumberBaseItem('sześćdziesiąt', 60),
            new NumberBaseItem('siedemdziesiąt', 70),
            new NumberBaseItem('osiemdziesiąt', 80),
            new NumberBaseItem('dziewięćdziesiąt', 90),

            new NumberBaseItem('sto', 100),
            new NumberBaseItem('dwieście', 200),
            new NumberBaseItem('trzysta', 300),
            new NumberBaseItem('czterysta', 400),
            new NumberBaseItem('pięćset', 500),
            new NumberBaseItem('sześćset', 600),
            new NumberBaseItem('siedemset', 700),
            new NumberBaseItem('osiemset', 800),
            new NumberBaseItem('dziewięćset', 900),

            new NumberAdvancedItem(new Thousand()),
            new NumberAdvancedItem(new Million()),
            new NumberAdvancedItem(new Billion()),
            new NumberAdvancedItem(new Trillion()),
        ]
    }
}
