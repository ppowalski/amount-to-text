import { AbstractNumber } from "../../numbers/AbstractNumber";

export class Billion extends AbstractNumber {
    getValue(): number {
        return 1000000000;
    }

    getSingle(): string {
        return "miliard";
    }

    getFew(): string {
        return "miliardy";
    }

    getMultiple(): string {
        return "miliardów";
    }
}
