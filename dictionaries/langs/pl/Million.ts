import { AbstractNumber } from "../../numbers/AbstractNumber";

export class Million extends AbstractNumber {
    getValue(): number {
        return 1000000;
    }

    getSingle(): string {
        return "milion";
    }

    getFew(): string {
        return "miliony";
    }

    getMultiple(): string {
        return "milionów";
    }
}
