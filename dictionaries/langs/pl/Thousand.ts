import { AbstractNumber } from "../../numbers/AbstractNumber";

export class Thousand extends AbstractNumber {
    getValue(): number {
        return 1000;
    }

    getSingle(): string {
        return "tysiąc";
    }

    getFew(): string {
        return "tysiące";
    }

    getMultiple(): string {
        return "tysięcy";
    }
}
