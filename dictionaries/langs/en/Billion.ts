import { AbstractNumber } from "../../numbers/AbstractNumber";

export class Billion extends AbstractNumber {
    getValue(): number {
        return 1000000000;
    }

    getSingle(): string {
        return "billion";
    }

    getFew(): string {
        return "billion";
    }

    getMultiple(): string {
        return "billion";
    }
}
