import { AbstractNumber } from "../../numbers/AbstractNumber";

export class Thousand extends AbstractNumber {
    getValue(): number {
        return 1000;
    }

    getSingle(): string {
        return "thousand";
    }

    getFew(): string {
        return "thousand";
    }

    getMultiple(): string {
        return "thousand";
    }
}
