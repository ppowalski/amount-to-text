import { AbstractNumber } from "../../numbers/AbstractNumber";

export class Trillion extends AbstractNumber {
    getValue(): number {
        return 1000000000000;
    }

    getSingle(): string {
        return "trillion";
    }

    getFew(): string {
        return "trillion";
    }

    getMultiple(): string {
        return "trillion";
    }
}
