import { AbstractNumber } from "../../numbers/AbstractNumber";

export class Million extends AbstractNumber {
    getValue(): number {
        return 1000000;
    }

    getSingle(): string {
        return "million";
    }

    getFew(): string {
        return "million";
    }

    getMultiple(): string {
        return "million";
    }
}
