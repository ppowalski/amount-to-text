import { DictionaryInterface } from "../../DictionaryInterface";
import { DictionaryItemInterface } from "../../DictionaryItemInterface";
import { NumberBaseItem } from "../../numbers/NumberBaseItem";
import { NumberAdvancedItem } from "../../numbers/NumberAdvancedItem";
import { Thousand } from "./Thousand";
import { Million } from "./Million";
import { Billion } from "./Billion";
import { Trillion } from "./Trillion";
import { Hundred } from "./Hundred";

export class Dictionary implements DictionaryInterface {
    getItems(): DictionaryItemInterface[] {
        return [
            new NumberBaseItem('zero', 0),
            new NumberBaseItem('one', 1),
            new NumberBaseItem('two', 2),
            new NumberBaseItem('three', 3),
            new NumberBaseItem('four', 4),
            new NumberBaseItem('five', 5),
            new NumberBaseItem('six', 6),
            new NumberBaseItem('seven', 7),
            new NumberBaseItem('eight', 8),
            new NumberBaseItem('nine', 9),

            new NumberBaseItem('ten', 10),
            new NumberBaseItem('eleven', 11),
            new NumberBaseItem('twelve', 12),
            new NumberBaseItem('thirteen', 13),
            new NumberBaseItem('fourteen', 14),
            new NumberBaseItem('fifteen', 15),
            new NumberBaseItem('sixteen', 16),
            new NumberBaseItem('seventeen', 17),
            new NumberBaseItem('eighteen', 18),
            new NumberBaseItem('nineteen', 19),

            new NumberBaseItem('twenty', 20),
            new NumberBaseItem('thirty', 30),
            new NumberBaseItem('forty', 40),
            new NumberBaseItem('fifty', 50),
            new NumberBaseItem('sixty', 60),
            new NumberBaseItem('seventy', 70),
            new NumberBaseItem('eighty', 80),
            new NumberBaseItem('ninety', 90),

            new NumberAdvancedItem(new Hundred),
            new NumberAdvancedItem(new Thousand),
            new NumberAdvancedItem(new Million),
            new NumberAdvancedItem(new Billion),
            new NumberAdvancedItem(new Trillion),
        ]
    }
}