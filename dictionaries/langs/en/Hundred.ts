import { AbstractNumber } from "../../numbers/AbstractNumber";

export class Hundred extends AbstractNumber {
    getValue(): number {
        return 100;
    }

    getSingle(): string {
        return "hundred";
    }

    getFew(): string {
        return "hundred";
    }

    getMultiple(): string {
        return "hundred";
    }
}
