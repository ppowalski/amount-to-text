import { DictionaryItemInterface } from "../DictionaryItemInterface";

export class NumberBaseItem implements DictionaryItemInterface {
    label: string;
    value: number;

    constructor(label: string, value: number) {
        this.label = label;
        this.value = value;
    }
}