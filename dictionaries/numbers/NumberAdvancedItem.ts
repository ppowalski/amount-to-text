import { DictionaryItemInterface } from "../DictionaryItemInterface";
import { VariationInterface } from "../../variations/VariationInterface";
import { AbstractNumber } from "./AbstractNumber";

export class NumberAdvancedItem implements DictionaryItemInterface {
    label: VariationInterface;
    value: number;

    constructor(number: AbstractNumber) {
        this.label = number;
        this.value = number.getValue();
    }
}
