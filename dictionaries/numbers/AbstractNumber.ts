import { VariationInterface } from "../../variations/VariationInterface";

export abstract class AbstractNumber implements VariationInterface {
    abstract getValue(): number;

    abstract getSingle(): string;

    abstract getFew(): string;

    abstract getMultiple(): string;
}
