import { VariationInterface } from "../variations/VariationInterface";

export interface DictionaryItemInterface {
    label: string|VariationInterface,
    value: number
}