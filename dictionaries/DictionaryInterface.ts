import { DictionaryItemInterface } from "./DictionaryItemInterface";

export interface DictionaryInterface {
    getItems: () => DictionaryItemInterface[],
}