import { Variation } from "./variations/Variation";
import { AbstractNumber } from "./dictionaries/numbers/AbstractNumber";
import { DictionaryInterface } from "./dictionaries/DictionaryInterface";
import { Dictionary } from "./dictionaries/langs/pl/Dictionary";
import { DictionaryItemInterface } from "./dictionaries/DictionaryItemInterface";

export class NumberToText {
    dictionaryItems: DictionaryItemInterface[];
    dictionaryItemsWithKeysAsValue: object;
    dictionaryKeys: number[];

    /**
     * @param dictionary
     */
    constructor(dictionary: DictionaryInterface = new Dictionary) {
        this.dictionaryItems = this.sortDictionaryItems(dictionary.getItems());
        this.dictionaryItemsWithKeysAsValue = {};
        this.dictionaryKeys = [];

        for(let item of this.dictionaryItems) {
            this.dictionaryKeys.push(item.value);
            this.dictionaryItemsWithKeysAsValue[item.value] = item;
        }
    }

    /**
     * @param items
     * @private
     */
    private sortDictionaryItems(items: DictionaryItemInterface[]): DictionaryItemInterface[] {
        return items.sort((a: DictionaryItemInterface, b: DictionaryItemInterface): number => {
            return a.value > b.value ? -1 : a.value < b.value ? 1 : 0;
        });
    }

    /**
     * @param number
     */
    convert(number): string {
        let textToReturn = '';

        if(number === 0) {
            if(typeof this.dictionaryItemsWithKeysAsValue[0].label === "string") {
                return this.dictionaryItemsWithKeysAsValue[0].label;
            }

            return this.dictionaryItemsWithKeysAsValue[0].label.getSingle();
        }

        for(let dictionaryItemValue of this.dictionaryKeys) {
            const int = Math.floor(number / dictionaryItemValue);
            const label = this.dictionaryItemsWithKeysAsValue[dictionaryItemValue].label;

            if(int > 0) {
                let base = '';

                if(int === 1) {
                    if(label instanceof AbstractNumber) {
                        base = this.dictionaryItemsWithKeysAsValue[1].label;
                    } else {
                        base = this.dictionaryItemsWithKeysAsValue[dictionaryItemValue].label;
                    }
                } else {
                    base = this.convert(int);
                }

                if(base.length > 0) {
                    textToReturn += ` ${base}`;
                }

                if(label instanceof AbstractNumber) {
                    textToReturn += ` ${Variation.getVariation(int, label)}`;
                    number -= (int * dictionaryItemValue);
                } else {
                    number -= dictionaryItemValue;
                }

                if(number <= 0) {
                    break;
                }
            }
        }

        return textToReturn.trim();
    }
}