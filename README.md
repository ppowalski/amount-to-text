# Amount to text
Author: Patryk Powalski

### Description
Package for converting the amount into text

### Methods
```
amountToText(amount: number, currency: CurrencyInterface (optional - default PLN), numberDict: DictionaryInterface (optional - default PL)): string
```
```
amountToTextAsObject(amount: number, currency: CurrencyInterface (optional - default PLN), numberDict: DictionaryInterface (optional - default PL)): object
```

### Basic list of currencies (you can define own)
```
    - PLN,
    - EUR,
    - USD,
    - CHF
```

### Basic number dictionaries (you can define own)
```
    - PL,
    - EN
```

#### Example
``` javascript
import { amountToText, amountToTextAsObject, currencies } from 'amount-to-text';

console.log(`Result: ${amountToText(10123.99)}`);
> "Result: dziesięć tysięcy sto dwadzieścia trzy złote dziewięćdziesiąt dziewięć groszy"

console.log(`Result: ${amountToText(10123.99, currencies.EUR)}`);
> "Result: dziesięć tysięcy sto dwadzieścia trzy euro dziewięćdziesiąt dziewięć centów"

console.log(`Result: ${amountToTextAsObject(10123.99, currencies.EUR)}`);
> "Result: {
    beforeDotNumber: 'dziesięć tysięcy sto dwadzieścia trzy',
    beforeDotCurrency: 'euro',

    afterDotNumber: 'dziewięćdziesiąt dziewięć',
    afterDotCurrency: 'centów'
}"
```