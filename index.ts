import { NumberToText } from "./NumberToText.js";
import { AmountInterface } from "./AmountInterface";
import { CurrencyInterface } from "./currencies/CurrencyInterface";
import { CurrencyManager } from "./currencies/CurrencyManager";
import { DictionaryInterface } from "./dictionaries/DictionaryInterface";
import { Dictionary } from "./dictionaries/langs/pl/Dictionary";
import allCurrencies from './currencies/currencies';
import allDictionaries from './dictionaries/dictionaries';

/**
 * Split amount to object
 * @param amount
 */
export const splitAmount = (amount: number): AmountInterface => {
    const result = amount
        .toFixed(2)
        .replace(',', '.')
        .split('.')
        .map(Number);

    return {
        beforeDotNumber: result[0],
        afterDotNumber: result[1]
    }
};

/**
 * Method to convert amount to text
 *
 * @param amount
 * @param currency
 * @param numberDict
 */
export const amountToTextAsObject = (amount: number, currency: CurrencyInterface = currencies.PLN, numberDict: DictionaryInterface = new Dictionary) => {
    const amountAsObject = splitAmount(amount);

    const numberToText = new NumberToText(numberDict);
    const beforeDotNumberAsText = numberToText.convert(amountAsObject.beforeDotNumber);
    const afterDotNumberAsText = numberToText.convert(amountAsObject.afterDotNumber);

    const currencyManager = new CurrencyManager(amountAsObject.beforeDotNumber, amountAsObject.afterDotNumber, currency);
    const currencyVariations = currencyManager.getVariations();

    return {
        beforeDotNumber: beforeDotNumberAsText,
        beforeDotCurrency: currencyVariations.beforeDotVariation,

        afterDotNumber: afterDotNumberAsText,
        afterDotCurrency: currencyVariations.afterDotVariation
    };
};

/**
 * Method to convert amount to text
 *
 * @param amount
 * @param currency
 * @param numberDict
 */
export const amountToText = (amount: number, currency: CurrencyInterface = currencies.PLN, numberDict: DictionaryInterface = new Dictionary) => {
    const result = amountToTextAsObject(amount, currency, numberDict);

    return `${result.beforeDotNumber} ${result.beforeDotCurrency} ${result.afterDotNumber} ${result.afterDotCurrency}`;
};

/**
 * List of default currencies
 */
export const currencies = allCurrencies;

/**
 * List of default dictionaries
 */
export const dictionaries = allDictionaries;