import { PLN } from "./PLN/PLN";
import { EUR } from "./EUR/EUR";
import { USD } from "./USD/USD";
import { CHF } from "./CHF/CHF";

export default {
    PLN: new PLN,
    EUR: new EUR,
    USD: new USD,
    CHF: new CHF,
};