import { CurrencyInterface } from "./CurrencyInterface";
import { CurrencyVariationInterface } from "./CurrencyVariationInterface";
import { Variation } from "../variations/Variation";

export class CurrencyManager {
    beforeDot: number;
    afterDot: number;
    currency: CurrencyInterface

    /**
     * @param beforeDot
     * @param afterDot
     * @param currency
     */
    constructor(beforeDot: number, afterDot: number, currency: CurrencyInterface) {
        this.beforeDot = beforeDot;
        this.afterDot = afterDot;
        this.currency = currency;
    }

    /**
     * Get currency variations (before dot and after dot)
     */
    getVariations(): CurrencyVariationInterface {
        return {
            beforeDotVariation: Variation.getVariation(this.beforeDot, this.currency.getVariationBeforeDot()),
            afterDotVariation: Variation.getVariation(this.afterDot, this.currency.getVariationAfterDot()),
        }
    }
}