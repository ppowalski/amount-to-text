import { VariationInterface } from "../variations/VariationInterface";

export interface CurrencyInterface {
    getVariationBeforeDot: () => VariationInterface;
    getVariationAfterDot: () => VariationInterface;
}