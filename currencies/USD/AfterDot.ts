import { VariationInterface } from "../../variations/VariationInterface";

export class AfterDot implements VariationInterface {
    getSingle(): string {
        return 'cent';
    }

    getFew(): string {
        return 'centy';
    }

    getMultiple(): string {
        return 'centów';
    }
}