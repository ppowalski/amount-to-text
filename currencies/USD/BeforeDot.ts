import { VariationInterface } from "../../variations/VariationInterface";

export class BeforeDot implements VariationInterface {
    getSingle(): string {
        return 'dolar';
    }

    getFew(): string {
        return 'dolary';
    }

    getMultiple(): string {
        return 'dolarów';
    }
}