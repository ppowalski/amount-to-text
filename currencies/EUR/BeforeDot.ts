import { VariationInterface } from "../../variations/VariationInterface";

export class BeforeDot implements VariationInterface {
    getSingle(): string {
        return 'euro';
    }

    getFew(): string {
        return 'euro';
    }

    getMultiple(): string {
        return 'euro';
    }
}