export interface CurrencyVariationInterface {
    beforeDotVariation: string,
    afterDotVariation: string,
}