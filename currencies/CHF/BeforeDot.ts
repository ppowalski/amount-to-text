import { VariationInterface } from "../../variations/VariationInterface";

export class BeforeDot implements VariationInterface {
    getSingle(): string {
        return 'frank';
    }

    getFew(): string {
        return 'franki';
    }

    getMultiple(): string {
        return 'franków';
    }
}