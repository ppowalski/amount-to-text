import { VariationInterface } from "../../variations/VariationInterface";

export class AfterDot implements VariationInterface {
    getSingle(): string {
        return 'centym';
    }

    getFew(): string {
        return 'centymy';
    }

    getMultiple(): string {
        return 'centymów';
    }
}