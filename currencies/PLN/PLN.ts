import { CurrencyInterface } from "../CurrencyInterface";
import { VariationInterface } from "../../variations/VariationInterface";
import { BeforeDot } from "./BeforeDot";
import { AfterDot } from "./AfterDot";

export class PLN implements CurrencyInterface {
    getVariationBeforeDot(): VariationInterface {
        return new BeforeDot;
    }

    getVariationAfterDot(): VariationInterface {
        return new AfterDot;
    }
}