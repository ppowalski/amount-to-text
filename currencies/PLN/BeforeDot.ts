import { VariationInterface } from "../../variations/VariationInterface";

export class BeforeDot implements VariationInterface {
    getSingle(): string {
        return 'złoty';
    }

    getFew(): string {
        return 'złote';
    }

    getMultiple(): string {
        return 'złotych';
    }
}