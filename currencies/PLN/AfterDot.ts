import { VariationInterface } from "../../variations/VariationInterface";

export class AfterDot implements VariationInterface {
    getSingle(): string {
        return 'grosz';
    }

    getFew(): string {
        return 'grosze';
    }

    getMultiple(): string {
        return 'groszy';
    }
}