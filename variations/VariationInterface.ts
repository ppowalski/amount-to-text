export interface VariationInterface {
    /**
     * Word as single variation
     */
    getSingle: () => string;

    /**
     * Word as few variation
     */
    getFew: () => string;

    /**
     * Word as multiple variation
     */
    getMultiple: () => string;
}