import { VariationInterface } from "./VariationInterface";

export class Variation {
    static getVariation(integer: number, variation: VariationInterface): string {
        const lastDigit = Number(String(integer).slice(-1));

        if(integer === 1) {
            return variation.getSingle();
        } else if((integer >= 5 && integer <= 14) || ([1, 5, 6, 7, 8, 9, 0].indexOf(lastDigit) !== -1)) {
            return variation.getMultiple();
        } else {
            return variation.getFew();
        }
    }
}